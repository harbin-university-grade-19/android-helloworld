package com.neuedu.android.helloworld;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    List<Fragment> fragments = new ArrayList<>();
    String tabTitls[] = {"车票","订单","我的"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //设置选项卡
        tabLayout = findViewById(R.id.tab);
        viewPager = findViewById(R.id.viewPager);
        
        
        //设置VIewPager之后，Tab的想象内容有ViewPager(适配器)提供,
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){
            @Override
            public void onTabSelected(@NonNull TabLayout.Tab tab) {
                setTitle(tab.getText());
                super.onTabSelected(tab);
            }
        });

        //填充选项内容
//        tabLayout.addTab(tabLayout.newTab().setText("车票"));
//        tabLayout.addTab(tabLayout.newTab().setText("订单"));
//        tabLayout.addTab(tabLayout.newTab().setText("@我的"));

        //SimpleAdatper  ArrayAdapter   自定义适配器


        //提前创建好三个 TextView  3
//        List<TextView> viewPagerItem = new ArrayList();
//        for (int i = 0; i < 3; i++) {
//            TextView tv = new TextView(this);
//            tv.setText("第"+i+"个页面内容");
//            tv.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
//            tv.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
////        tv.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
//            tv.setGravity(Gravity.CENTER);
//            viewPagerItem.add(tv);
//        }


//        for (int i = 0; i < 3; i++) {
//            fragments.add(new TicketFragment());
//        }
        fragments.add(new TicketFragment());
        fragments.add(new OrderFragment());
        fragments.add(new MyFragment());

//        viewPager.setAdapter(new PagerAdapter(){
//
//            //告诉VIewPager一共有几个页面
//            @Override
//            public int getCount() {
//                return viewPagerItem.size();
//            }
//
//            @Override
//            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
//                return view == object;
//            }
//
//
//            //用于真正设置具体某一个位置的视图
//            @NonNull
//            @Override
//            public Object instantiateItem(@NonNull ViewGroup container, int position) {
//                //container -- ViewPager
//                TextView item = viewPagerItem.get(position);
//                container.addView(item);//将对应位置的TextView 放到VIewPager中
//                return item;
//            }
//
//            @Override
//            public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
//                ViewPager vp = (ViewPager) container;
//                vp.removeView((View)object);
//                System.out.println(container);
//                System.out.println(position);
//                System.out.println(object);
//            }
//        });

        // FragmentManager ,
        viewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragments.get(0).onActivityResult(requestCode, resultCode, data);
    }

    class MyFragmentPagerAdapter extends  FragmentPagerAdapter{


        public MyFragmentPagerAdapter(@NonNull FragmentManager fm) {
            super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitls[position];
        }
    }

    //双击   2秒以内
    //记录第一次点击的时间
    //获取当前时间 1653873348494
        // 录第一次点击的时间 1653874348494
        //再次点击： 1653874348494
//                1)  结束当前activity
//                 2）1653874348494



    long startTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {



        if(keyCode == KeyEvent.KEYCODE_BACK){
            long current = System.currentTimeMillis();
            Log.d("MainActivity","startTime:"+startTime);
            Log.d("MainActivity","current:"+current);
            Log.d("MainActivity","(current-startTime):"+(current-startTime));
            if(current-startTime <1500){
                finish();
            }else{
                startTime =  current;
            }

        }

        Toast.makeText(this, "keyCode: "+keyCode, Toast.LENGTH_SHORT).show();
        
        return true;
    }
    
    
    
    
    
}