package com.neuedu.android.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.neuedu.android.helloworld.my.ConcatActivity;

public class MyActivity extends AppCompatActivity implements View.OnClickListener
   , AdapterView.OnItemClickListener
{
    Button btnConcat;
    Button btnAccount;
    Button btnPassword;
    Button btnLogout;
    ListView lvMain;
    ArrayAdapter  adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
//        btnConcat = findViewById(R.id.btnConcat);
//        btnAccount = findViewById(R.id.btnAccount);
//        btnPassword = findViewById(R.id.btnPassword);

        lvMain = findViewById(R.id.lvMain);
        btnLogout = findViewById(R.id.btnLogout);

//        btnConcat.setOnClickListener(new BtnOnClickListener());
//        btnAccount.setOnClickListener(new BtnOnClickListener());
//        btnPassword.setOnClickListener(new BtnOnClickListener());
//        btnLogout.setOnClickListener(new BtnOnClickListener());

//        btnConcat.setOnClickListener(this);
//        btnAccount.setOnClickListener(this);
//        btnPassword.setOnClickListener(this);

        //定义一个列表的显示内容
        String[] datas = new String[]{
                "我的联系人",
                "我的账户",
                "我的密码"
        };



        //准备适配器
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,datas);
//        adapter.notifyDataSetChanged();//如果内容有变化需要调用适配器的方法，通知ListView刷新

        //将适配器绑定到 ListView
        lvMain.setAdapter(adapter);

        //给ListView的每个条目添加点击事件
        lvMain.setOnItemClickListener(this);


        btnLogout.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        //根据点击的组件
        int id = v.getId();
        //根据按钮去顶意图
        Intent intent = null;
        switch (id){
//            case R.id.btnConcat:
//                intent = new Intent(this,ConcatActivity.class);
//                break;
//            case R.id.btnAccount:
//                intent = new Intent(this,AccountActivity.class);
//                break;
//            case R.id.btnPassword:
//                intent = new Intent(this,PasswordActivity.class);
//                break;
            case R.id.btnLogout:
                intent = new Intent(this,LoginActivity.class);
                logout();
                break;
        }

        //如果是退出登录，需要结束当前Activity
        if(id == R.id.btnLogout){
            finish();//
        }

        startActivity(intent);

    }

    /**
     * 退出清空配置文件
     */
    private void logout() {

        SharedPreferences sp = getSharedPreferences("AUTO_LOGIN", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.commit();

    }


    //ListVIew条目点击的时间

    /**
     *  String[] datas = new String[]{
     *                 "我的联系人",
     *                 "我的账户",
     *                 "我的密码"
     *         };
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("MainActivity","点击的position"+position);
        Intent intent = null;
        switch (position){
            case 0:
                intent = new Intent(this, ConcatActivity.class);
                break;
            case 1:
                intent = new Intent(this,AccountActivity.class);
                break;
            case 2:
                intent = new Intent(this,PasswordActivity.class);
                break;
        }
        startActivity(intent);

    }

//    class BtnOnClickListener implements  View.OnClickListener {
//        @Override
//        public void onClick(View v) {
//            //根据点击的组件
//            int id = v.getId();
//            //根据按钮去顶意图
//            Intent intent = null;
//            switch (id){
//                case R.id.btnConcat:
//                    intent = new Intent(MainActivity.this,ConcatActivity.class);
//                    break;
//                case R.id.btnAccount:
//                    intent = new Intent(MainActivity.this,AccountActivity.class);
//                    break;
//                case R.id.btnPassword:
//                    intent = new Intent(MainActivity.this,PasswordActivity.class);
//                    break;
//                case R.id.btnLogout:
//                    intent = new Intent(MainActivity.this,LoginActivity.class);
//                    break;
//            }
//
//            startActivity(intent);
//
//        }
//    }


}