package com.neuedu.android.helloworld;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.neuedu.android.helloworld.my.ConcatActivity;

public class MyFragment extends Fragment  implements View.OnClickListener , AdapterView.OnItemClickListener

{
    Button btnConcat;
    Button btnAccount;
    Button btnPassword;
    Button btnLogout;
    ListView lvMain;
    ArrayAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my,container,false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        获取Ativity
        lvMain = getActivity().findViewById(R.id.lvMain);
        btnLogout = getActivity().findViewById(R.id.btnLogout);

        //定义一个列表的显示内容
        String[] datas = new String[]{
                "我的联系人",
                "我的账户",
                "我的密码"
        };


        //准备适配器
        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,datas);

        //将适配器绑定到 ListView
        lvMain.setAdapter(adapter);
        //给ListView的每个条目添加点击事件
        lvMain.setOnItemClickListener(this);
        btnLogout.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        //根据点击的组件
        int id = v.getId();
        //根据按钮去顶意图
        Intent intent = null;
        switch (id){
            case R.id.btnLogout:
                intent = new Intent(getActivity(),LoginActivity.class);
                logout();
                break;
        }

        //如果是退出登录，需要结束当前Activity
        if(id == R.id.btnLogout){
            getActivity().finish();//
        }

        startActivity(intent);

    }

    /**
     * 退出清空配置文件
     */
    private void logout() {

        SharedPreferences sp = getActivity().getSharedPreferences("AUTO_LOGIN", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.commit();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("MainActivity","点击的position"+position);
        Intent intent = null;
        switch (position){
            case 0:
                intent = new Intent(getActivity(), ConcatActivity.class);
                break;
            case 1:
                intent = new Intent(getActivity(),AccountActivity.class);
                break;
            case 2:
                intent = new Intent(getActivity(),PasswordActivity.class);
                break;
        }
        startActivity(intent);

    }
}
