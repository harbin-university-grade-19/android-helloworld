package com.neuedu.android.helloworld.common;

import com.neuedu.android.helloworld.entity.Passanger;

import java.util.List;

public class CommonResult {
    private Integer code;
    private String message;
    private List<Passanger> obj;

    public CommonResult() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Passanger> getObj() {
        return obj;
    }

    public void setObj(List<Passanger> obj) {
        this.obj = obj;
    }
}