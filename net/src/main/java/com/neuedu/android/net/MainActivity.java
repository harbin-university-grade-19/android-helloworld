package com.neuedu.android.net;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {



    Button btnLogin;

    Handler  handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RequestLogin().start();
            }
        });
    }

    class RequestLogin extends Thread{
        @Override
        public void run() {
            try {
                URL url = new URL("https://blog.csdn.net/qq_44720366/article/details/106217358");
                URLConnection connection = url.openConnection();
                BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));                StringBuffer stringBuffer = new StringBuffer();
                char[] bts = new char[1024];
                int len = -1;
                while(   (len = inputStream.read(bts))!= -1){
                    stringBuffer.append(bts,0,len);
                }
                System.out.println("stringBuffer: "+stringBuffer.toString());
                Log.d("net",stringBuffer.toString());
//                Toast.makeText(MainActivity.this, "content:"+content, Toast.LENGTH_SHORT).show();



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}