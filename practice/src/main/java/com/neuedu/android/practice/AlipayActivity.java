package com.neuedu.android.practice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AlipayActivity extends AppCompatActivity {

    private ListView lvAliPayList;
    private SimpleAdapter adapter  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alipay);
        lvAliPayList = findViewById(R.id.lvAliPayList);

        String objects[] = new String[]{"顾客消息","生活号","蚂蚁财富","车险","生日提醒","天天收羊毛","天天抽奖"};
        int drawableIds[] = {R.drawable.bank,R.drawable.call_out,R.drawable.info};
        Random rnd = new Random();



        List<Map<String,Object>> data= new ArrayList();
        for (int i = 0; i < objects.length; i++) {
            //构造每一行的数据
            Map row = new HashMap();
            row.put("pic",drawableIds[rnd.nextInt(3)]);
            row.put("title",objects[i]);
            row.put("time",1+""+i+":"+0+i);
            data.add(row);
        }
        
//        adapter = new ArrayAdapter(this, R.layout.list_item)
//        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,objects);
        String from[] = new String[]{"pic", "title","time"};   // 从data中某一行的Map中哪个Key取数据
        int[] to = new int[]{R.id.pic, R.id.title ,R.id.time};        //将去除来的数据放到单行布局中的那个组件中
        adapter = new SimpleAdapter(this, data,R.layout.list_item,from,to);

        lvAliPayList.setAdapter(adapter);

    }
}