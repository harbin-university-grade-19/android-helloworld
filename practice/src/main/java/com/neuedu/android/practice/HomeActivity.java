package com.neuedu.android.practice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String SHARED_PRE_FILE = "Home_Mine" ;
    ListView lvHome;
    private SimpleAdapter adapter;
    List<Map<String, Object>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //使用Java程序填充ListView
        lvHome = findViewById(R.id.lvHome);
        data = new ArrayList();

        /**
         * - 我的账号
         * - 邮箱
         * - 昵称
         * - 手机号
         * - 性别
         */
        SharedPreferences sp = getSharedPreferences(SHARED_PRE_FILE, Context.MODE_PRIVATE);

        //我的账号
        Map row1 = new HashMap();
        row1.put("title", "我的账号");
        row1.put("value",  sp.getString("account", "jshand"));
        row1.put("src", R.drawable.forward_25);
        data.add(row1);

        Map row2 = new HashMap();
        row2.put("title", "邮箱");
        row2.put("value", "zhangjinsa@189.com");
        row2.put("src", R.drawable.forward_25);
        data.add(row2);

        Map row3 = new HashMap();
        row3.put("title", "昵称");
        row3.put("value", "金山老师");
        row3.put("src", R.drawable.forward_25);
        data.add(row3);

        Map row4 = new HashMap();
        row4.put("title", "手机号");
        row4.put("value", "13888888888");
//        row4.put("src",R.drawable.forward_25);
        data.add(row4);

        Map row5 = new HashMap();
        row5.put("title", "性别");
        row5.put("value", sp.getString("gender", "男"));
        row5.put("src", R.drawable.forward_25);
        data.add(row5);

        adapter = new SimpleAdapter(this, data, R.layout.activity_home_list_item,
                new String[]{"title", "value", "src"},
                new int[]{R.id.title, R.id.value, R.id.src});


        lvHome.setAdapter(adapter);
        lvHome.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, "点击的位置:" + position, Toast.LENGTH_SHORT).show();
        switch (position) {
            case 0:
                modifyAccount();
                break;
            case 4:
                modifyGender();
                break;
        }
    }

    /**
     * 修改账号
     */
    private void modifyAccount() {
        Map<String, Object> row1 = data.get(0);
        String account = (String) row1.get("value"); //jshand
        EditText et = new EditText(this);
        et.setText(account);
        et.setSingleLine();//单行输入
        et.setSelection(0, account.length()); //选中文字 ,方便删除

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("修改账号");
        builder.setView(et);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String editAccount = et.getText().toString();
                Toast.makeText(HomeActivity.this, "editAccount:" + editAccount, Toast.LENGTH_SHORT).show();
                //将修改之后的内容文本放到 data
                row1.put("value", editAccount);

                //通知ListView数据发生变化了，重新渲染ListView的内容
                adapter.notifyDataSetChanged();
                //将信息保存到SharedPreference
                SharedPreferences sp = getSharedPreferences(SHARED_PRE_FILE, Context.MODE_PRIVATE);

                //Home_Mine  /data/data/package_name/shres
                SharedPreferences.Editor edit = sp.edit();
                edit.putString("account",editAccount);
                edit.commit();
            }
        });
        Dialog dialog = builder.show();

    }


    /**
     * 修改性别
     */
    private void modifyGender() {
        Map<String, Object> row4 = data.get(4);
//        String gender = (String) row4.get("value"); //jshand
        String genders[] = { "男","女"  };


        Dialog dialog = new AlertDialog.Builder(this)
                .setTitle("设置性别")
                .setItems(genders, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        row4.put("value",genders[which]);
                        //通知ListView数据发生变化了，重新渲染ListView的内容
                        adapter.notifyDataSetChanged();
                        //Home_Mine  /data/data/package_name/shres
                        SharedPreferences sp = getSharedPreferences(SHARED_PRE_FILE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = sp.edit();
                        edit.putString("gender",genders[which]);
                        edit.commit();
                    }
                })
                .show();

    }
}