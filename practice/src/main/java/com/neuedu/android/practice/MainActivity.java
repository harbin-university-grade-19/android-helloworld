package com.neuedu.android.practice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLinearLayout;
    Button btnCallActivity;
    Button btnAliPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //通过引用的ID找到按钮
        btnLinearLayout = findViewById(R.id.btnLinearLayout);
        btnCallActivity = findViewById(R.id.btnCallActivity);
        btnAliPay = findViewById(R.id.btnAliPay);


        //给按钮添加事件 监听器
        btnLinearLayout.setOnClickListener(this);
        btnCallActivity.setOnClickListener(this);
        btnAliPay.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Button btn = (Button) v;
        Toast.makeText(MainActivity.this, "按钮被点击:" + btn.getText(), Toast.LENGTH_SHORT).show();

        //通过 Class的形式指定对应的 Activity（apk）
        Class clazz = null;
        switch (v.getId()) {
            case R.id.btnLinearLayout:
                clazz = LinearLayoutActivity.class;
                break;
            case R.id.btnCallActivity:
                clazz = CallActivity.class;
                break;
            case R.id.btnAliPay:
                clazz = AlipayActivity.class;
                break;
        }

        //显示意图
        Intent intent = new Intent(this, clazz);
        //打开Activity
        startActivity(intent);
    }


//    class MyOnClickListener implements  View.OnClickListener{
//
//        @Override
//        public void onClick(View v) {
//            Button btn = (Button) v;
//            Toast.makeText(MainActivity.this, "按钮被点击:" + btn.getText(), Toast.LENGTH_SHORT).show();
//        }
//    }
    
    
    
    //渲染菜单
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //使用渲染器 通过布局文件的形式渲染菜单
        getMenuInflater().inflate( R.menu.menu_main ,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        String title = item.getTitle().toString();
        Toast.makeText(this, "选中的Title\r\n"+title, Toast.LENGTH_SHORT).show();
        //通过 Class的形式指定对应的 Activity（apk）
        Class clazz = null;
        switch (item.getItemId()) {
            case R.id.menuLinear:
                clazz = LinearLayoutActivity.class;
                break;
            case R.id.menuCall:
                clazz = CallActivity.class;
                break;
            case R.id.menuAlipay:
                clazz = AlipayActivity.class;
                break;
        }

        //显示意图
        Intent intent = new Intent(this, clazz);
        //打开Activity
        startActivity(intent);

        return true;
    }
}