package com.neuedu.android.practice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MyViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_view);
    }
}