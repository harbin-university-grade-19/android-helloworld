package com.neuedu.android.practice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class NeueduActivity extends AppCompatActivity {

    private ListView lvJob;
    private SimpleAdapter adapter;
    private List<Map<String,Object>> data= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neuedu);

        String[] job = {"质检员","Java开发工程师","C++","嵌入式工程师"};
        String[] salary = {"5k-10k","12k+","面谈","年薪20k"};
        String[] company = {"东软教育集团","中科软股份有限公司","阳光保险集团","国寿财险集团"};
        String[] address = {"沈阳","北京","上海","深圳"};

        lvJob = findViewById(R.id.lvJob);

        data = new ArrayList();
        Random rnd = new Random();
        for (int i = 0; i < 10; i++) {
            Map map = new HashMap<>();
            map.put("job", job[rnd.nextInt(4)]);
            map.put("salary", salary[rnd.nextInt(4)]);
            map.put("company", company[rnd.nextInt(4)]);
            map.put("address", address[rnd.nextInt(4)]);
            data.add(map);
        }

        adapter = new SimpleAdapter(
                this,
                data,
                R.layout.activity_neuedu_list_item,
                new String[]{"job","salary","company","address"},
                new int[]{R.id.tvJob,R.id.tvSalary,R.id.tvCompany,R.id.tvAddress});

        lvJob.setAdapter(adapter);



    }
}