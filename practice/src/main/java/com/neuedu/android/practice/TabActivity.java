package com.neuedu.android.practice;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class TabActivity extends AppCompatActivity   {

    TabLayout tab;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        tab = findViewById(R.id.tab);
        viewPager = findViewById(R.id.viewPager);


        tab.setupWithViewPager(viewPager);

        List<Fragment> list = new ArrayList();
        for (int i = 0; i < 5; i++) {
            list.add(new OneFragment());
        }

        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager(),FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return "标题"+position;
            }
        });
//        viewPager.setAdapter(new PagerAdapter(){
//            @Override
//            public int getCount() {
//                return 4;
//            }
//
//            @Override
//            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
//                return view == object;
//            }
//
//            @NonNull
//            @Override
//            public Object instantiateItem(@NonNull ViewGroup container, int position) {
//                TextView tv = new TextView(TabActivity.this);
//                tv.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//                tv.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
//                tv.setGravity(Gravity.CENTER);
//                tv.setText("第"+position+"视图");
//                container.addView(tv);
//                return tv;
//            }
//
//            @Nullable
//            @Override
//            public CharSequence getPageTitle(int position) {
//                return "第"+position+"个";
//            }
//
//            @Override
//            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
////                super.destroyItem(container, position, object);
//            }
//        });

    }


}