package com.neuedu.android.practice.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class LetterListView extends View {

    String letters[]  = {"a","b","c","d","........."};



    public LetterListView(Context context) {
        super(context);
    }

    public LetterListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LetterListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LetterListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Canvas :画布
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        int height = getHeight();
        int width = getWidth();
        super.onDraw(canvas);
        Paint pen = new Paint();
        pen.setColor(Color.BLACK);//黑色的比
        pen.setTextSize(80);
        //设置画布的颜色
        canvas.drawColor(Color.GRAY);//填充一个灰色的颜色（相当于是背景）
        canvas.drawText("自定义组件",0,30,pen);

        canvas.drawText("宽: "+width,0,100,pen);
        canvas.drawText("高: "+height,0,200,pen);

        String letters[]  = {"a","b","c","d","........."};
        //  a   0 * height/26
        //  b   1* height/26
        //  c   2* height/26
        //  d   3* height/26
        for (int i = 0; i < letters.length; i++) {

            canvas.drawText( letters[i],0,i* height/26,pen);
        }


    }
}
